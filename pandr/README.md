# Visualize frequencies selected food words from "*Parks and Recreations*"

This directory aims to visualize selected food words from the TV show ["*Parks and Recreations*"](https://en.wikipedia.org/wiki/Parks_and_Recreation).

## Requirements

Python & Jupyter were used. 

In CLI, do:

```bash
pip install -r requirements.txt
```

Additionally, Inkscape was used to assemle and annotate figures.

## Data

The data file `parks-and-recreation_scripts.csv` was obtained using [`sf2`](https://codeberg.org/penguinsfly/springfield-scraper) that downloaded the scripts from [Springfield! Springfield!](https://www.springfieldspringfield.co.uk)

```bash
sf2 --show "parks-and-recreation" --format csv
```
