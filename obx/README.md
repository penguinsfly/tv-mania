# Visualize the number of "You know" in "*Outer Banks*" Season 3

This directory aims to visualize the number of "*you know*" from Season 3 of the TV show ["*Outer Banks*"](https://en.wikipedia.org/wiki/Outer_Banks_(TV_series)).

## Requirements

Python & Jupyter were used. 

In CLI, do:

```bash
pip install -r requirements.txt
```

## Data

The data file `outer-banks-2020_scripts.json` was obtained using [`sf2`](https://codeberg.org/penguinsfly/springfield-scraper) that downloaded the scripts from [Springfield! Springfield!](https://www.springfieldspringfield.co.uk).

```bash
sf2 --show "outer-banks-2020" --season 3 --format json
```
