# Visualize the number of flashes in the TV show "*Chuck*"

This directory aims to visualize the number of flashes from the the TV show ["*Chuck*"](https://en.wikipedia.org/wiki/Chuck_(TV_series)) based on Reddit comments and TV transcripts

## Requirements

Python & Jupyter were used. 

In CLI, do:

```bash
pip install -r requirements.txt
```

## Data

To get all the data and the processed data for this notebook, please run:

```bash
make scrape # scrape both tv scripts and libreddit
make process # process the data to prepare for visualization
```
