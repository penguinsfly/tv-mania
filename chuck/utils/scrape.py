"""This scraes libreddit for comments with number of flashes"""
import os
import re
import asyncio
import pickle

import rich
import rich.progress

import httpx
import requests
from bs4 import BeautifulSoup


def request(url):
    r = requests.get(url)
    assert r.status_code == 200
    s = BeautifulSoup(r.content, 'lxml')
    return s


def collect_episode_urls():
    source_url = 'https://libreddit.tiekoetter.com'
    query_url = source_url + '/r/chuck/search?q=Chuck+versus+the+rewatch'
    list_soup = request(query_url)
    rich.print(f'Collecting discussion episode URLs from {query_url} ...')
    
    ep_urls = []
    for post in list_soup.find_all(class_='post'):
        title = post.find(class_='post_title').a.text
        if not title.lower().startswith('chuck versus the rewatch: season'):
            continue

        for link in post.find_all('li'):
            ep_urls.append(source_url + link.p.a.get('href'))

    num_urls = len(ep_urls)
    rich.print(f'\t Found total of [bold green]{num_urls}[/] episode links.')
    
    return ep_urls


def find_author(p):
    author = p.find_parent(class_='comment_right')\
        .find('a', class_='comment_author')\
        .get_text()
    return author


async def scrape_post(client, url, progress, task):
    regex_pattern = re.compile('(number|no\.).+\d+', re.IGNORECASE)
    
    response = await client.get(url)
    soup = BeautifulSoup(response.content, "lxml")
    
    title = soup.find(class_='post_title').text.strip()
    comments = [
        dict(author = find_author(p), comment = p.text)
        for p in soup.find_all('p', text=regex_pattern)
    ]
    
    data = dict(url = url, title = title, comments = comments)
    
    if progress is not None:
        assert task is not None
        progress.update(task, advance=1)

    return data


async def scrape(urls):
    pbar_args = [
        rich.progress.TextColumn(
            "[green]{task.description}[/green]", table_column=rich.table.Column(ratio=1)
        ),
        "[white][progress.percentage]{task.percentage:>3.0f}%[/white]",
        rich.progress.BarColumn(
            bar_width=None, table_column=rich.table.Column(ratio=2)
        ),
        rich.progress.TextColumn(
            "[green][{task.completed}/{task.total}][/green]",
            table_column=rich.table.Column(ratio=1),
        ),
        rich.progress.SpinnerColumn(table_column=rich.table.Column(ratio=1))
    ]
    
    client_kargs = dict(    
        transport=httpx.AsyncHTTPTransport(retries=5),
        limits=httpx.Limits(
            max_keepalive_connections=20,
            max_connections=20,
            keepalive_expiry=5,
        ),
        timeout=httpx.Timeout(20)
    )
    
    with rich.progress.Progress(*pbar_args) as progress:
        task = progress.add_task("Requesting ...", total=len(urls))

        async with httpx.AsyncClient(**client_kargs) as client:
            data = await asyncio.gather(*[
                asyncio.ensure_future(scrape_post(client, url, progress, task))
                for url in urls
            ])
            
    rich.print("Finished scraping.")

    return data


if __name__ == '__main__':
    # Collect URLs of episodes of rewatch
    ep_urls = collect_episode_urls()
    
    # Collect comments and the data in each episode
    assert len(ep_urls) > 0, "Empty list of URLs"
    ep_data = asyncio.run(scrape(ep_urls))

    # Save data
    data_path = r"data/chuck_libreddit.pkl"
    data = dict(
        episode_urls = ep_urls,
        episode_data = ep_data
    )

    with open(data_path, "wb") as f:
        pickle.dump(data, f)
