import os
import re

import pickle
import numpy as np
import pandas as pd

### Process data from libreddit to get number of flashes/zooms

def parse_title(title):
    ep_id = re.search('Discussion.+S(?P<season>\d{1,2})E?(?P<episode>\d{1,2})',title, re.IGNORECASE)
    if ep_id is None:
        print(title)
    ep_id = {k: int(v) for k, v in ep_id.groupdict().items()}
    return ep_id

def parse_comment(comment):
    if pd.isna(comment):
        return [{'count': np.nan}]
    
    c = comment.lower()
    p_word = r'(num(ber)|no\.)\s+(i|o)f\s+(?P<word>flashes|zooms)'
    
    # single flasher, flasher first then num. flashes
    p1 = p_word + r'\s*\((?P<flasher>\w+)\)\s*(:|-)\s*(?P<count>\d+)'
    d = re.search(p1, c)
    
    if d is not None:
        d = [d.groupdict()]
        return d
    
    # multiple flashers, num. flashes then flasher
    p2 = r'(?P<count>\d+)\s*(?P<type>[^\(]+)?\((?P<flasher>[^\)]+)\)'

    const = re.search(p_word, c)
    assert const is not None
    const = const.groupdict()
    
    d = [{**const, **m.groupdict()} for m in re.finditer(p2, c)]
    assert len(d) > 0
    return d


with open(r"data/chuck_libreddit.pkl", "rb") as f:
    data = pickle.load(f)
    
df = pd.DataFrame(data['episode_data']).explode('comments')

df = pd.concat([
    df.drop(columns='comments'),
    df['comments'].apply(pd.Series).dropna(how='all',axis=1)
], axis=1).drop_duplicates()

df = pd.concat([
    df['title'].apply(parse_title).apply(pd.Series),
    df['comment'].apply(parse_comment).to_frame('content')
], axis=1)\
.drop_duplicates(subset=['season','episode'])\
.explode('content')

df = pd.concat([
    df.drop(columns='content'),
    df['content'].apply(pd.Series)
], axis=1)\
.drop_duplicates()\
.sort_values(by=['season','episode'])\
.reset_index(drop=True)

df.to_csv('data/proc_libreddit.csv', index=None)


### Process transcripts to count number of words 
def count_words(script):
    script = script.lower()
    script_flash = len(re.findall(r'\bflash', script))
    script_zoom = len(re.findall(r'\bzoom', script))
    return dict(script_flash=script_flash, 
                script_zoom=script_zoom)

scripts = pd.read_json("data/chuck_scripts.json")
scripts = pd.concat([
    scripts,
    scripts['script'].apply(count_words).apply(pd.Series)
], axis=1)\
.filter(regex='(season|episode$|script_.*)')

scripts.to_csv('data/proc_scripts.csv', index=None)
