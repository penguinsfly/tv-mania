# TV Mania

Simple analysis and visualizations of TV shows/movies & their scripts.

Simply, the 2D story experience curiosity.

Repository: <https://codeberg.org/penguinsfly/tv-mania>

View notebooks at: <https://penguinsfly.codeberg.page/tv-mania>

